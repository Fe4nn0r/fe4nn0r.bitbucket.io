import React, { PureComponent } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';

import { postMessage } from '../api';
import Loader from './Loader';

class PostMessageForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      text: '',
      isPublic: false,
      isSendingMessage: false,
    };

    this.handleChangeMessageToCreateText = this.handleChangeMessageToCreateText.bind(
      this
    );
    this.handleChangeMessageToCreateVisibility = this.handleChangeMessageToCreateVisibility.bind(
      this
    );
    this.handleCreateMessageFormSubmit = this.handleCreateMessageFormSubmit.bind(
      this
    );
  }

  handleChangeMessageToCreateText(evt) {
    this.setState({
      text: evt.target.value,
    });
  }

  handleChangeMessageToCreateVisibility(evt) {
    this.setState({
      isPublic: evt.target.checked,
    });
  }

  handleCreateMessageFormSubmit(evt) {
    evt.preventDefault();
    const { isPublic, text } = this.state;

    postMessage(text, isPublic).then(messageFromApi => {
      this.setState({
        isPublic: false,
        text: '',
      });

      this.props.onMessageCreated(messageFromApi);
    });
  }

  render() {
    const { isSendingMessage, isPublic, text } = this.state;
    return (
      <form method="POST" onSubmit={this.handleCreateMessageFormSubmit}>
        <div className="form-group">
          <label htmlFor="create-message-text-input">Contenu du message</label>
          <textarea
            rows="3"
            className="form-input"
            id="create-message-text-input"
            onChange={this.handleChangeMessageToCreateText}
            value={text}
            required
            minLength={1}
          />
        </div>

        <div className="form-group">
          <input
            type="checkbox"
            className="form-input"
            id="create-message-visibility-input"
            checked={isPublic}
            onChange={this.handleChangeMessageToCreateVisibility}
            placeholder="Public"
          />
          <label htmlFor="create-message-visibility-input">
            Visible publiquement
          </label>
        </div>

        <div className="form-group">
          {!isSendingMessage && <button type="submit">Envoyer</button>}

          {isSendingMessage && <Loader />}
        </div>
      </form>
    );
  }
}

PostMessageForm.propTypes = {
  onMessageCreated: PropTypes.func.isRequired,
};

export default PostMessageForm;
