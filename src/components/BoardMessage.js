import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';

const ICON_PREFIX = process.env.NODE_ENV === 'production' ? '/build' : '';

function BoardMessage({ username, avatar, text, isPublic }) {
  return (
    <li className="message">
      <span className="message__content">{text}</span>
      <span className="message__user-information">
        <span className="message__avatar">
          <img src={avatar} alt={username} width="30" height="30" />
        </span>
        <span className="message__username">{username}</span>
      </span>

      {isPublic && (
        <span className="message__visibility">
          <img
            src={`${ICON_PREFIX}/icons/eye-outline.png`}
            height="12"
            width="12"
            alt="Public"
          />
          <span>Public</span>
        </span>
      )}

      {!isPublic && (
        <span className="message__visibility">
          <img
            src={`${ICON_PREFIX}/icons/eye-off-outline.png`}
            height="12"
            width="12"
            alt="Privé"
          />

          <span>Privé</span>
        </span>
      )}
    </li>
  );
}

BoardMessage.propTypes = {
  username: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  isPublic: PropTypes.bool.isRequired,
};

export default BoardMessage;
