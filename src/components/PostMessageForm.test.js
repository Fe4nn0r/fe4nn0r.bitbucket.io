import React from 'react';
import { mount } from 'enzyme';

import PostMessageForm from './PostMessageForm';

jest.useFakeTimers();

describe('PostMessageForm', () => {
  it('should display the message', () => {
    const fakeFunction = jest.fn();
    const renderedComponent = mount(
      <PostMessageForm onMessageCreated={fakeFunction} />
    );

    expect(renderedComponent.exists('textarea')).toBeTruthy();
    expect(renderedComponent.exists("input[type='checkbox']")).toBeTruthy();
    expect(renderedComponent.exists("button[type='submit']")).toBeTruthy();
  });

  it('should call the onMessageCreated function on submit', done => {
    const fakeFunction = jest.fn();
    const renderedComponent = mount(
      <PostMessageForm onMessageCreated={fakeFunction} />
    );

    renderedComponent
      .find('textarea')
      .simulate('change', { target: { value: 'Lorem ipsum dolor' } });
    renderedComponent.find("input[type='checkbox']").simulate('click');
    renderedComponent.find('form').simulate('submit');

    jest.runOnlyPendingTimers();

    setImmediate(() => {
      renderedComponent.update();
      expect(fakeFunction).toBeCalled();
      expect(fakeFunction).toHaveBeenCalledTimes(1);

      const mock = fakeFunction.mock.calls[0][0];
      expect(typeof mock.username !== 'undefined').toBeTruthy();
      expect(typeof mock.avatar !== 'undefined').toBeTruthy();
      expect(typeof mock.id !== 'undefined').toBeTruthy();
      expect(mock.text).toEqual('Lorem ipsum dolor');
      expect(mock.isPublic).toBeFalsy();
      done();
    });
  });
});
