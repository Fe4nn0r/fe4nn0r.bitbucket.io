import React from 'react';

import { mount } from 'enzyme';

import BoardMessage from './BoardMessage';

describe('BoardMessage', () => {
  it('should display the public message', () => {
    const renderedComponent = mount(
      <BoardMessage
        username="USERNAME"
        avatar="https://img.com/avatar"
        text="Lorem ipsum dolor"
        isPublic
      />
    );

    expect(renderedComponent.contains('USERNAME')).toBe(true);
    expect(renderedComponent.contains('Lorem ipsum dolor')).toBe(true);
    expect(
      renderedComponent
        .find('img')
        .at(0)
        .prop('src')
    ).toEqual('https://img.com/avatar');

    expect(renderedComponent.contains('Public')).toBe(true);
  });

  it('should display the private message', () => {
    const renderedComponent = mount(
      <BoardMessage
        username="USERNAME"
        avatar="https://img.com/avatar"
        text="Lorem ipsum dolor"
        isPublic={false}
      />
    );

    expect(renderedComponent.contains('Privé')).toBe(true);
  });
});
