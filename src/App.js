import React, { Component, Fragment } from 'react';
import './style/App.scss';
import BoardMessage from './components/BoardMessage';
import PostMessageFrom from './components/PostMessageForm';
import Loader from './components/Loader';

import { fetchMessageList } from './api';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messageList: [],
      isLoadingMessageList: false,
    };

    this.handleMessageCreated = this.handleMessageCreated.bind(this);
  }

  componentDidMount() {
    this.setState({
      isLoadingMessageList: true,
    });
    fetchMessageList().then(messageList => {
      this.setState({
        messageList,
        isLoadingMessageList: false,
      });
    });
  }

  handleMessageCreated(message) {
    this.setState(prevState => {
      const messageList = prevState.messageList.slice(0);
      messageList.push(message);
      return {
        messageList,
      };
    });
  }

  render() {
    const { isLoadingMessageList, messageList } = this.state;
    return (
      <div className="message-board">
        <h1 className="message-board__title">Kiklox Message Board</h1>

        {isLoadingMessageList && (
          <div className="message-board__loader">
            <Loader />
          </div>
        )}

        {!isLoadingMessageList && (
          <Fragment>
            <ul className="message-board__message-list">
              {messageList.map(({ id, username, avatar, text, isPublic }) => (
                <BoardMessage
                  key={id}
                  username={username}
                  avatar={avatar}
                  text={text}
                  isPublic={isPublic}
                />
              ))}
            </ul>

            <PostMessageFrom onMessageCreated={this.handleMessageCreated} />
          </Fragment>
        )}
      </div>
    );
  }
}

export default App;
