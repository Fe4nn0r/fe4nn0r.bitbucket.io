import faker from 'faker';

let currentId = 1;
const timeArray = [200, 300, 150, 250, 2000, 3000, 1000, 1500];

function getRandomTimer() {
  return timeArray[Math.floor(timeArray.length * Math.random())];
}

function generateFakeMessage() {
  return {
    username: faker.internet.userName(),
    avatar: faker.internet.avatar(),
    text: faker.lorem.paragraph(),
    isPublic: Math.random() >= 0.5,
    id: currentId++,
  };
}

function generateFakeMessageList() {
  const messageList = [];

  for (let i = 0; i < 2; i++) {
    messageList.push(generateFakeMessage());
  }

  return messageList;
}

export function fetchMessageList() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(generateFakeMessageList());
    }, getRandomTimer());
  });
}

export function postMessage(text, isPublic) {
  const messageFromApi = {
    username: faker.internet.userName(),
    avatar: faker.internet.avatar(),
    text,
    isPublic,
    id: currentId++,
  };

  return new Promise(resolve => {
    setTimeout(() => {
      resolve(messageFromApi);
    }, getRandomTimer());
  });
}
